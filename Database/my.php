<?php

$input = filter_input_array(INPUT_POST);

include_once 'connect.php';

if (mysqli_connect_errno()) {
  echo json_encode(array('mysqli' => 'Failed to connect to MySQL: ' . mysqli_connect_error()));
  exit;
}

if ($input['action'] == 'edit') {
    $mysqli->query("UPDATE requests SET invoice='" . $input['invoice'] . "' WHERE request_id='" . $input['request_id'] . "'");
    $mysqli->query("UPDATE requests SET notes='" . $input['notes'] . "' WHERE request_id='" . $input['request_id'] . "'");
} 
else if ($input['action'] == 'delete') {
    $mysqli->query("DELETE from requests WHERE request_id='" . $input['request_id'] . "'");
} 
 
mysqli_close($mysqli);

echo json_encode($input);