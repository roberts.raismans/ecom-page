## Used languages and methods ##

    HTML & CSS - fundementals of the WEB APP;
    PHP - Communication with the database;
    Javascript - Responsive features;
    jQuery AJAX - Inline editable tables ("Tabledit" and "Datatable" plugins);
    Boostrap - responsive design, fonts, blocks;
    *Other style and script plugins*
    
## Project made with such programms ##

    VS Code- Code writing and editing;
    GIT and GITLAB- releases and backup;
    WAMPserver- Virtual development and server environment (PHP, Database, apache,...);
    MySQL- Database.
    
## MySQL database ##

Database is named "mydb" and it consists of 3 tables. First table is "register"- it holds information about system users (name, surname, banc acc, user id) and is also linked with
the second table, which is named "requests". Second table only holds info about requests that are sent to the server via request form. Third table holds user HASHED passwords
for the login system.

## Login system ##

Every single page of this project requires user to be logged in succesfully to be able to view its content. Login works with MySQL database, PHP and Hashed passwords. Every attempt
to log in is checked, whether entered information is valid. There are 2 types of users- ADMIN and Casual user. Administrator can see and work with all the pages of the APP, but
casual users dont have acces to pages "Review Requests" and "Payment". Login works with sessions and session variables which are used to show correct information in the tables
and determine wether user is admin or casual user.

Password are Hashed using **BCRYPT** one way function. 

## Pages and functionality ##

You can find information about each page of the app and request status meaning in the "About" section of the APP.

## In-Line editable tables ##

You will find in-line editable tables in 3 sections of this app- "My Requests", "Review Request" and "Payment". In "My requests" user will see only requests made with his User ID.
User can also edit and save info about "Invoice" and "Notes" in real time and also see status of all of his requests. In "Review Requests" administrator reviews and changes
the status of the request. In "Payment" section table is no longer editable, but consists of information from 2 tables (Query). There is also a button named "PAY ALL" that
makes payments for all accepted requests (changes their status to 10).


## Fallowing things are achieved ##

- WEB Project is almost fully mobile optimized (however it would most likely be used only on work laptops);
- Cloud hosting is implemented in such way- I am hosting this project from my personal working station (with virtual server);
- I will present this app in my real life work project- that means design is kept as minimalistic as possible.

