<!DOCTYPE html>
<?php
// We need to use sessions, so always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
	header('Location: index.html');
	exit;
}
?>
<html>
   
   <head>
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
       <link rel="stylesheet" type="text/css" href="style/style.css">
       <link rel="stylesheet" type="text/css" href="style/accordion.css">     
       <script src="Javascript/script.js"></script>

       <title>About</title>
       
   </head>

   <body>
       <div class="header">
           <h1 class="header_title">Robert's Expense System v1.0</h1>
       </div>

       <div class="topnav" id="myTopnav">
           <a href="new" ><i class="fa fa-fw fa-plus"></i>New Request</a>
           <a href="myrequests.php"><i class="fa fa-fw fa-archive"></i>My Requests</a>
           <a href="review.php"><i class="fa fa-fw fa-search"></i>Review Requests</a>
           <a href="pay.php"><i class="fa fa-fw fa-money"></i>PAYMENT</a>
           <a href="about.php" class="active"><i class="fa fa-fw fa-info"></i>About</a>
           <a href="login/logout.php"><i class="fa fa-fw fa-sign-out"></i>LOG OUT</a>
           <a href="javascript:void(0);" class="icon" onclick="navEffect()">
           <i class="fa fa-bars"></i>
           </a>
       </div>
<div class="faq">
    <h2>INFO about system</h2>
        <h4>Basic things to know before starting your work with this expense system!</h4>
        <button class="accordion">Sections of the system:</button>
            <div class="panel">
                <p>
                    <span class="good key">"NEW REQUESTS"-</span>
                     Every logged in user can submit their expense requests. You have to chose the persons USER ID, then select the TYPE of your expense. Don't forget to add mendatory link to specific invoice on our server or else your request will be denied. If you need to add some additional comments you can freely do so in the "Additional notes" section of the form!
                    <br/> </br>
                </p>
                <p>
                    <span class="good key">"MY REQUESTS"-</span>
                    In this section logged in user can view only requests made for him (based on his USER ID). When viewing your requests, you can see requests ID and Status. If your request has been sent for corrections or you have made mistakes during making your request, you have the  
                    <span class="keyword"> ability to change your requests invoice and notes.</span>
                    You can do so by clicking on the tables edit icon on the right side of the table.
                    <br/> </br>
                </p>
                <p>
                    <span class="bad key">"REVIEW REQUESTS"-</span>
                    (ONLY FOR ADMIN). In this section of the system user who is administrator will adjust the status of your requests, based on his review results and verdict!
                    <br/> </br>
                </p>
                <p>
                    <span class="bad key">"PAYMENT"-</span>
                    (ONLY FOR ADMIN). In this section all approved requests will recieve payment, based on the form that you filled out. Also the status of your request will be changed to 10.
                </p> 
            </div>

        <button class="accordion">Status meaning</button>
            <div class="panel">
                <p>
                    <span class="bad key"> "0"-</span>
                    Youy request has been denied. There is no need to edit it, because it is deleted and wont be accepted in the system anymore. You can look for additional comments why your request got denied in the "Notes" section of your request!
                </p> <br/> </br>
                <p>
                    <span class="keyword"> "1"-</span>
                    Default status for any new request that has not been reviewed yet.
                </p> <br/> </br>
                <p>
                    <span class="bad key"> "2"-</span>
                    Some information is missing- fix your request by adding all necessary information!
                </p> <br/> </br>
                <p>
                    <span class="bad key"> "3"-</span>
                    Wrong type of expense- fix your request by correcting the type of expense!
                </p> <br/> </br>
                <p>
                    <span class="bad key"> "4"-</span>
                    Bad invoice- fix your request by correcting the invoice (it may be poor quality, for wrong entry etc)!
                </p> <br/> </br>
                <p>
                    <span class="good key"> "5"-</span>
                    Your request has been accepted- wait for the payment!
                </p> <br/> </br>
                <p>
                    <span class="good key"> "10"-</span>
                    Your request has been paid succesfully- check that you recieved your funds!
                </p> <br/> </br>


            </div>

        <button class="accordion">Other FAQ</button>
            <div class="panel">
                <p>
                    <span class="keyword key">How long does it take to review my requests? </span>
                    - Requests are usually reviewed in 1-2 working days.
                </p> <br/> </br>
                <p>
                    <span class="keyword key">How long does it take to recieve the funds? </span>
                    - It depends on your bank. If you are using **** bank, you will recieve funds as soon as payment is sent succesfully. Otherwise it can take 1-2 more working days.
                </P> <br/> </br>
                <p>
                    <span class="keyword key">Is this the final version of the expense system? </span>
                    - NO. This is just a fundemental test version. Some features will possibly be changed, based on user reviews and recomendations. Design will also be ajusted accordingly.
                </p> <br/> </br>
                <p>
                    <span class="keyword key">Can I make requests for other users? </span>
                    In this stage of the systems test any user with login available can make requests also for other people. This feature will be most likely changed in further stages of development.
                </p> <br/> </br>
            </div>
</div>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>      
     
</body>
