<?php
session_start();
include_once '../Database/connect.php';

// Now we check if the data from the login form was submitted, isset() will check if the data exists.
if ( !isset($_POST['username'], $_POST['password']) ) {
	// Could not get the data that should have been sent.
    exit('Please fill both the username and password fields!');
}

// Prepare our SQL, preparing the SQL statement will prevent SQL injection.
if ($stmt = $mysqli->prepare('SELECT id, password FROM accounts WHERE username = ?')) {
	// Bind parameters (s = string, i = int, b = blob, etc).
    $stmt->bind_param('i', $_POST['username']);
	$stmt->execute();
	// Store the result so we can check if the account exists in the database.
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        $stmt->bind_result($id, $password);
        $stmt->fetch();
        // Account exists, now we verify the password.
        if (password_verify($_POST['password'], $password)) {
            // Verification success! User has loggedin!
            // Create sessions so we know the user is logged in.
            session_regenerate_id();
            $_SESSION['loggedin'] = TRUE;
            $_SESSION['name'] = $_POST['username'];
            $_SESSION['id'] = $id;

            echo '<script language="javascript">';
            echo 'alert("Welcome User: ' . $_SESSION['name'] . '!")';
            echo '</script>';
        } else {
            echo '<script language="javascript">';
            echo 'alert("Incorrect password- Try again!")';
            echo '</script>';
        }
    } else {
        echo '<script language="javascript">';
        echo 'alert("Incorrect username- Try again!")';
        echo '</script>';
    }
    $stmt->close();
header("refresh:1; url=../myrequests.php");
exit;
}



?>