<!DOCTYPE html>
<?php
// We need to use sessions, so always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
	header('Location: index.html');
	exit;
}
?>

<html>
   
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <script src="Javascript/script.js"></script>

        <title>New Request</title>  
    </head>

    <body>
        <div class="header">
            <h1 class="header_title">Robert's Expense System v1.0</h1>
        </div>

        <div class="topnav" id="myTopnav">
            <a href="new" class="active"><i class="fa fa-fw fa-plus"></i>New Request</a>
            <a href="myrequests.php"><i class="fa fa-fw fa-archive"></i>My Requests</a>
            <a href="review.php"><i class="fa fa-fw fa-search"></i>Review Requests</a>
            <a href="pay.php"><i class="fa fa-fw fa-money"></i>PAYMENT</a>
            <a href="about.php"><i class="fa fa-fw fa-info"></i>About</a>
            <a href="login/logout.php"><i class="fa fa-fw fa-sign-out"></i>LOG OUT</a>
            <a href="javascript:void(0);" class="icon" onclick="navEffect()">
            <i class="fa fa-bars"></i>
            </a>
        </div>

        <div class="form">
        <h5>Logged in as user: <?php print_r($_SESSION['name']); ?> </h5>
            <form method="POST" action="Database/form.php">
        
                <label for ="UserID" class="label1">User ID</label>
                    <select class="form_input" name="UserID" id="UserID">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>     
                <label for ="type" class="label1">Type of expenses</label>
                    <select class="form_input form_input_select" name="type">
                        <option>Fuel</option>
                        <option>Business trip</option>
                        <option>Service (LV)</option>
                        <option>Service (Abroad)</option>
                        <option>Other</option>
                    </select>
                <label for ="incoice" class="label1">Invoice (Link)</label>
                    <input type="text" class="form_input" name="invoice">
                <label for ="notes" class="label1">Additional notes</label>
                    <input type="text" class="form_input" name="notes">
                <button type="submit" class="button" name="btn-save">Register</button>
        </form> </div>
    </body>
</html>