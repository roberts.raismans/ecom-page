<?php
// We need to use sessions, so always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
	header('Location: index.html');
	exit;
}
else if (($_SESSION['name'] != 1)){
    echo '<script language="javascript">';
    echo 'alert("YOU DONT HAVE ACCES TO THIS PAGE!")';
    echo '</script>';
    header("refresh:1; url= myrequests.php");
    exit;
}
$mysqli = new mysqli('localhost', 'root', 'admin123', 'mydb','3308');
$query = "SELECT * FROM requests INNER JOIN register ON requests.user=register.user_id WHERE status =5 ORDER BY request_id ASC";
$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
?>
<html>  
 <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="style/style.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
          <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">
          <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
          <script src="Javascript/jquery.tabledit.min.js"></script>
          <script src="Javascript/script.js"></script>

        <title>Request Review</title>  
    </head>  
    <body>
        <div class="header">
            <h1 class="header_title">Robert's Expense System v1.0</h1>
        </div>

        <div class="topnav" id="myTopnav">
            <a href="new" ><i class="fa fa-fw fa-plus"></i>New Request</a>
            <a href="myrequests.php"><i class="fa fa-fw fa-archive"></i>My Requests</a>
            <a href="review.php" ><i class="fa fa-fw fa-search"></i>Review Requests</a>
            <a href="pay.php" class="active"><i class="fa fa-fw fa-money"></i>PAYMENT</a>
            <a href="about.php"><i class="fa fa-fw fa-info"></i>About</a>
            <a href="login/logout.php"><i class="fa fa-fw fa-sign-out"></i>LOG OUT</a>
            <a href="javascript:void(0);" class="icon" onclick="navEffect()">
            <i class="fa fa-bars"></i>
            </a>
        </div>    
    
  <div class="container">  
   <br />  
   <br />  
   <br />  
   <div class="panel-body">
            <div class="table-responsive">
    <h5>Logged in as user: <?php print_r($_SESSION['name']); ?> </h5> 
    <h3 align="center">Send payments to Approved requests!</h3><br />  
    <table id="editable_table" class="table table-bordered table-striped">
     <thead>
      <tr>
       <th>Request ID</th>
       <th>User ID</th>
       <th>Name</th>
       <th>Surname</th>
       <th>Account</th>
       <th>Type</th>
       <th>Invoice</th>
       <th>Notes</th>
       <th>Status</th>
      </tr>

     </thead>
     <tbody>
     <?php
     while($row = mysqli_fetch_array($result))
     {
      echo '
      <tr>
       <td>'.$row["Request_ID"].'</td>
       <td>'.$row["User"].'</td>
       <td>'.$row["Name"].'</td>
       <td>'.$row["Surname"].'</td>
       <td>'.$row["Account"].'</td>
       <td>'.$row["Type"].'</td>
       <td>'.$row["Invoice"].'</td>
       <td>'.$row["Notes"].'</td>
       <td>'.$row["Status"].'</td>
      </tr>
      ';
     }
     ?>
     </tbody>
    </table>
   </div>  
  </div>
    </div>
    <div class="form">
        <form method="POST" action="Database/sendpay.php">
        <input type="text" class="form_input" name="setstatus" value="PAY for all above requests:" disabled>
        <button type="submit" class="button" name="btn-pay">PAY all</button>
    </form></div>

 </body>  
</html>  
<script>  
$(document).ready(function(){  
    $('#editable_table').DataTable({
      "paging"      : false,
      "search"      : false,
    })
     $('#editable_table').Tabledit({
      url:'Database/tableEdit.php',
      columns:{
       identifier:[0, 'request_id'],
      },
      processing: true,
      restoreButton:false,
      deleteButton:false,
      onSuccess:function(data, textStatus, jqXHR)
      {
       if(data.action == 'delete')
       {
        $('#'+data.id).remove();
       }
      }
     });
     

 
});  
 </script>
